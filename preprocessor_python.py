import logging
import re

def find_macros(lines):
	macros = {}
	out_lines = []
	python_lines = []

	for line_number,line in enumerate(lines):
		original_line = line
		if re.search("#", line): #matching label
			match = re.search("#(.*)$",line)
			if match:
				python_lines.append(match.group(1))
				line = re.sub("(#.*)", "", line)
			else:
				raise SyntaxError('Invalid preprocessor directive\nLine '+str(line_number)+': '+original_line)
		out_lines.append(line)

	python_lines = '\n'.join(python_lines)
	exec(python_lines, macros)
	macros = dict( [(x,y) for x,y in macros.items() if not x.startswith('__')] )
	return macros, out_lines

def run_macros(macros, lines):
	out_lines = []
	for line_number,line in enumerate(lines):
		while True:
			for k,v in macros.items():
				if re.search(k, line):
					if hasattr(v, '__call__'):
						call = re.search(k+"\(.*?\)", line)
						if not call:
							raise TypeError("Using a macro function with malformed parameters")
						try:
							line = re.sub(k+"\(.*?\)", str(eval(call.group(0), macros)), line)
						except TypeError as err:
							raise TypeError("The macro {0}() takes {1} positional arguments".format(k, v.__code__.co_argcount)) from err
						except NameError as err:
							raise NameError("Unknown name in {0}".format(line)) from err
					else:
						line = re.sub(k, str(v), line)
					break
			else:
				break
		out_lines.append(line)

	return out_lines

def parse_python(lines):
	macros, lines = find_macros(lines)

	logging.debug("Macros found:")
	for k,v in macros.items():
		logging.debug("\t{0}: {1}".format(k,v))
	logging.debug("")

	logging.debug("File before python macros:")
	for line_number,line in enumerate(lines):
		if line:
			logging.debug("{0}\t{1}".format(line_number,line))
	logging.debug("")

	out_lines = run_macros(macros, lines)

	logging.debug("File after python macros:")
	for line_number,line in enumerate(out_lines):
		if line:
			logging.debug("{0}\t{1}".format(line_number,line))
	logging.debug("")

	return out_lines
