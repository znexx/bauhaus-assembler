# Bauhaus-assembler
## Introduction
Bauhaus-assembler is an assembler for the Bauhaus computer architecture,
written entirely in python. For now, this assembler uses Intel syntax
(I guess it should be configurable later?)
## Usage
```
assembler.py [arguments] [file ...]

Arguments:
	-o <file name>	Output binary with specified filename
	-v		Verbose mode
	-h		This help?
```
and it produces

See the [wiki](https://bitbucket.org/znexx/bauhaus-assembler/wiki/) for more details