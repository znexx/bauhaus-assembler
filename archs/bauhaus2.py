import logging
import binascii
import re
import sympy

"""
Converts lexed format to bauhaus1 binary
"""

instructions = {
	'dw':   (0x00, -1),
	'nop':  (0x00, 0),
	'in':   (0x01, 2),
	'out':  (0x02, 2),
	'halt': (0x03, 0),
	'mov':  (0x04, 2),

	'add': (0x10, 2),
	'sub': (0x11, 2),
	'mul': (0x12, 2),
	'div': (0x13, 2),
	'mod': (0x14, 2),
	'shl': (0x15, 2),
	'shr': (0x16, 2),
	'and': (0x17, 2),
	'or':  (0x18, 2),
	'xor': (0x19, 2),

	'jmp': (0x20, 1),
	'je':  (0x21, 3),
	'jne': (0x22, 3),
	'jg':  (0x23, 3),
	'jge': (0x24, 3),
}

def convert(intermediate):
	"""
	Parses a list of instructions in lexed format, and returns a bytearray containing the corresponding binary codes.
	"""
	binary = bytearray()
	current_addr = 0
	label_addresses = {}
	for line_number, line in enumerate(intermediate):
		for label in line[0]:
			label_addresses[label] = current_addr
		line[0] = current_addr
		current_addr = current_addr + calculate_size(line_number, line)
	logging.debug("Calculated addresses for labels: {}".format(label_addresses))

	for line_number, line in enumerate(intermediate):
		[address, mnemonic, operands] = line
		instruction_bytes = bytearray()
		if mnemonic == 'dw':
			for n, arg in enumerate(operands):
				instruction_bytes.append(sympy.sympify(arg))
		else:

			instruction_bytes.append(instructions[mnemonic][0])
			if instructions[mnemonic][1] != len(operands):
				raise SyntaxError("Wrong number of operands for instruction at {}: {}".format(str(line_number), line))
			for n, arg in enumerate(operands):
				if mnemonic == 'out' and n == 1: #second operand of port is constant
					instruction_bytes.append(sympy.sympify(arg) % 256)
					continue
				operands[n] = sympy.sympify(arg).subs(label_addresses) - address
				if not isinstance(operands[n], sympy.numbers.Integer):
					raise SyntaxError("Could not evaluate expression at {}: {}".format(str(line_number), operands[n]))
				if (operands[n] < -128) or (operands[n] > 127):
					raise ValueError("Address out of range at {}: {}".format(str(line_number), operands[n]))
				instruction_bytes.append(operands[n] % 256)

		logging.info("{0}".format(line))
		binary.extend(instruction_bytes)
	logging.info(binascii.hexlify(binary))
	return binary

def calculate_size(line_number, line):
	if line[1] == 'dw':
		return len(line[2])
	elif line[1] in instructions:
		return 1 + instructions[line[1]][1]
	else:
		raise SyntaxError("Invalid mnemonic at {}: {}".format(str(line_number), line[2]))
