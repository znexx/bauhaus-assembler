import binascii
import re
import sympy

"""
Converts lexed format to bauhaus1 binary
"""
instructions = {
	'add' : [0x30, ['o','i','i']],
	'and' : [0x18, ['o','i','i']],
	'or'  : [0x10, ['o','i','i']],
	'shl' : [0x40, ['o','i','i']],
	'shr' : [0x48, ['o','i','i']],
	'sub' : [0x38, ['o','i','i']],
	'xor' : [0x20, ['o','i','i']],

	'in'  : [0xa0, ['o','i']],
	'out' : [0xa8, ['i','i']],

	'je'  : [0x80, ['i','i','i']],
	'jg'  : [0x82, ['i','i','i']],
	'jge' : [0x83, ['i','i','i']],
	'jne' : [0x81, ['i','i','i']],

	'nop' : [0x00, []], 

	#pseudo instructions
	'db'  : [0x00, ['n']],
	'dw'  : [0x00, ['n']],
	'jmp' : [0x00, ['i']],
	'mov' : [0x00, ['o','i']],

	#special instructions
	'print_regs' : [0xf0, []],
	'print_ram'  : [0xf1, ['i','i']],
	'kill_emulator' : [0xf2, []],
}

def convert(intermediate):
	"""
	Parses a list of instructions in lexed format, and returns a bytearray containing the corresponding binary codes.
	"""
	#TODO: implement conversion of [mnemonic, [operands]]
	#format into bytecode, don't forget labels
	binary = bytearray()
	for line_number, line in enumerate(intermediate):
		instruction_bytes = bytearray()
		spec_val = 0
		operands = bytearray()

		instruction = instructions[line[1]]
		opcode = instruction[0]
		print("{0} {1}:".format(line[1], ", ".join(line[2])))

		spec_want = instruction[1]
		if len(spec_want) == len(line[2]):
			for operand_num, operand_str in enumerate(line[2]):
				try:
					operand_spec, operand_bytes = parse_operand(operand_str, instruction[1][operand_num])
				except SyntaxError as err:
					raise SyntaxError("Operand parsing error: {0}".format(err)) from err
				spec_val = (spec_val << 3) | operand_spec
				operands.extend(operand_bytes)
				a=0#print("\t\tspec: {0}, value: {1}".format(bin(operand_spec), binascii.hexlify(operand_bytes)))
			a=0#print("\topcode: {0} spec: {1} ({2}), operand value: {3}".format(hex(opcode), hex(spec_val), bin(spec_val), binascii.hexlify(operands)))
		else:
			raise SyntaxError("Wrong number of operands to instruction. {0} takes {1} operands: {2}".format(line[1], len(spec_want), line))

		instruction_bytes.append((opcode << 1) | ((spec_val & 0x100) >> 8))
		instruction_bytes.append(spec_val & 0xff)
		instruction_bytes.extend(operands)

		a=0#print("\tinstruction: {0}".format(binascii.hexlify(instruction_bytes)))

		binary.extend(instruction_bytes)
	print(binascii.hexlify(binary))
	return binary

def parse_operand(operand, io):
	"""
	Takes a string and returns an operand specifier in the range 0-7, and the operand in 1-3 bytes.
	"""
	spec = 0
	value = bytearray()

	memref = re.search("^\[(.*)\]$", operand)
	if memref:
		spec = spec | 0x04
		operand = memref.group(1)

	expr = sympy.simplify(operand)
	expr_dict = expr.as_coefficients_dict()

	a=0#print("\t{0}: {1}".format(io, str(expr)))

	if len(expr_dict) < 1 or len(expr_dict) > 2:
		raise SyntaxError("Wrong number of arguments in operand: {0}".format(expr))

	try:
		const = expr_dict.pop(1)
	except KeyError:
		a=0#print("\t\tNo constant")
	else:
		const = int(const)
		if const >= -0x8000 or const <= 0x7fff:
			a=0#print("\t\tConstant: {0}: {1}, {2}".format(hex(const), hex((const >> 8) & 0xff), hex(const & 0xff)))
			spec = spec | 0x02
			value.append((const >> 8) & 0xff)
			value.append(const & 0xff)
		else:
			raise ValueError("Constant value out of range")

	keys = list(expr_dict.keys())

	for item in keys:
		if not re.match("r[0-9]+", str(item)):
			a=0#print("ERROR:\tInvalid name: {0}".format(item))
			#raise NameError("Invalid name: {0}".format(item))
			return spec, value

	try:
		[reg, nreg] = expr_dict.popitem()
		if nreg != 1:
			raise ValueError("Coefficient for register did not evaluate to one")
		reg_num = int(re.search("[0-9]+", str(reg)).group(0))
	except KeyError:
		a=0#print("\t\tNo register")
	else:
		a=0#print("\t\tRegister {0}".format(reg_num))
		spec = spec | 0x01
		value.append(reg_num)

	try:
		[reg, nreg] = expr_dict.popitem()
		if nreg != 1:
			raise ValueError("Coefficient for register did not evaluate to one")
		reg_num = int(re.search("[0-9]+", str(reg)).group(0))
	except KeyError:
		a=0#print("\t\tNo 2:nd register")
	else:
		a=0#print("\t\t2:nd register {0}".format(reg_num))
		spec = spec & 0x4
		value.append(reg_num)

	return spec, value
