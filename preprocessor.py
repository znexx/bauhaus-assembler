import preprocessor_commands
import preprocessor_python

def parse(lines):
	try:
		lines = preprocessor_commands.parse_commands(lines)
	except SyntaxError as err:
		raise SyntaxError("Command preprocessor error: {0}".format(err)) from err
	try:
		out_lines = preprocessor_python.parse_python(lines)
	except (SyntaxError, TypeError, NameError) as err:
		raise RuntimeError("Python preprocessor error: {0}".format(err)) from err
		
	return out_lines
