import re

def tokenize(lines):
	out_lines = []
	labels = []
	for line_number,line in enumerate(lines):
		label = ""
		mnemonic = ""
		oper_list = []
		original_line = line

		if re.search("\S+:", line): #matching label
			match = re.match("^([a-zA-Z_]\w*):", line)
			if match:
				label = match.group(1)
				line = re.sub("\w+:\s*", "", line)
			else:
				raise SyntaxError('Invalid label at {}: {}'.format(str(line_number), original_line))

		if re.search("\w+", line): #matching mnemonic
			match = re.match("(\w+)\s*", line)
			if match:
				mnemonic = match.group(1).lower()
				line = re.sub("^\w+\s*","", line)
			else:
				raise SyntaxError('Invalid mnemonic at {}: {}'.format(str(line_number), original_line))
			oper_list = re.split("\s*,\s*",line)
			if len(oper_list) == 1 and oper_list[0] == "":
				oper_list.pop()
		out_lines.append([[label], mnemonic, oper_list])
	lines = out_lines
	for line in lines:
		if line[1]:
			if line[0]:
				labels.extend(line[0])
			labels = list(filter(None, labels))
			line[0] = labels
			labels = []
		else:
			labels.extend(line[0])
	out_lines = list(filter(lambda item: item[1], out_lines))

	return out_lines
