	jmp start
counter:	dw 32
inc:		dw 1
max:		dw 128
result:		dw 0

start:
	out counter, 1

	add inc, counter
	je counter, max, done
	jmp start
done:
	halt
