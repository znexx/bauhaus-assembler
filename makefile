SHELL=/bin/bash
PYDOC=pydoc3.4
PYDOCFLAGS=-w
PYFILES=$(wildcard *.py)
MODULENAMES=$(PYFILES:.py=)
MV=mv

AS=./assembler.py
CMP=cmp
TESTFILES_ASM=$(wildcard tests/*.asm)
TESTFILES_BIN=$(TESTFILES_ASM:.asm=.bin)

F_RED=\e[31m
F_GREEN=\e[32m
F_BOLD=\e[1m
F_RESET=\e[0m

docs:
	$(PYDOC) $(PYDOCFLAGS) $(MODULENAMES)
	$(MV) *.html doc/

test: $(TESTFILES_BIN)

$(TESTFILES_BIN): $(TESTFILES_ASM)
	@$(AS) $(@:.bin=.asm) -o $@
	@if $(CMP) -s $@ $(@:.bin=.correct); then \
		echo -e "$(@:.bin=.asm) $(F_BOLD)$(F_GREEN)OK!$(F_RESET)" ;\
	else \
		echo -e "$(@:.bin=.asm) $(F_BOLD)$(F_RED)FAIL!$(F_RESET)" ;\
	fi
	@echo

clean:
	rm -f *.html *.pyc
	rm -f tests/*.bin
