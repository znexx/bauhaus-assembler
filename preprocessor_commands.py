import logging
import re

def find_commands(lines):
	macros = {}
	out_lines = []

	for line_number,line in enumerate(lines):
		out_lines.append(line)

	return macros, lines

def run_commands(macros, lines):
	out_lines = []
	for line_number,line in enumerate(lines):
		##logging.debug("{0} {1}".format(line_number,line))
		out_lines.append(line)

	return out_lines


def parse_commands(lines):
	commands, lines = find_commands(lines)

	out_lines = run_commands(commands, lines)

	return out_lines
