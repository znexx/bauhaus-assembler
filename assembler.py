#!/usr/bin/python3

import sys
import logging

import file_handler
import preprocessor
import lexer
import archs.bauhaus2 as converter

def handle_file(filename):
	logging.debug("Reading file {0}".format(filename))
	try:
		source_lines = file_handler.read(filename)
	except IOError as err:
		raise RuntimeError("File reading error: {}".format(err))

	logging.debug("Preprocessing {0}".format(filename))
	try:
		source_lines = preprocessor.parse(source_lines)
	except (SyntaxError,TypeError) as err:
		raise RuntimeError("Preprocessor error: {}".format(err))

	logging.debug("Tokenizing {0}".format(filename))
	try:
		intermediate = lexer.tokenize(source_lines)
	except SyntaxError as err:
		raise RuntimeError("Lexer error: {}".format(err))

	return intermediate

if __name__ == "__main__":
	logging.basicConfig(level=logging.INFO)

	in_file = ""
	out_file = ""
	arguments = sys.argv
	arguments.reverse()
	arguments.pop()
	while arguments:
		arg = arguments.pop()
		if arg == "-o":
			try:
				out_file = arguments.pop()
			except IndexError as err:
				logging.critical("No filename given to -o")
				sys.exit(1)
		elif arg == "-q":
			logging.getLogger().setLevel(logging.ERROR)
		elif arg == "-v":
			logging.getLogger().setLevel(logging.DEBUG)
		else:
			in_file = arg

	if in_file == "":
		logging.critical("No input file specified".format())
		sys.exit(1)

	if out_file == "":
		logging.critical("No output file specified".format())
		sys.exit(1)

	try:
		intermediate = handle_file(in_file)
	except RuntimeError as err:
		logging.critical("File handling error in {}: {}".format(in_file, err))
		sys.exit(2)

	for line_number,line in enumerate(intermediate):
		logging.debug("{0} {1}".format(line_number,line))

	try:
		binary = converter.convert(intermediate)
	except (SyntaxError, ValueError) as err:
		logging.critical("Parsing error in {}: {}".format(in_file, err))
		sys.exit(3)

	logging.debug("Writing binary file {}".format(out_file))

	try:
		file_handler.write(out_file, binary)
	except IOError as err:
		logging.critical("File writing error in {}: {}".format(in_file, err))
		sys.exit(4)

# TODO:
# - modular assembler (preprocessor, in-assembly-functions, binary writer)
# - mnemonics list
# - segments?
# - include statements
# - preprocessor macros (python syntax, ofc)

#preprocessor:
# evaluate expressions
# remove comments, make code super nice
#lexer
# one instruction per line

# assembler
# save referencesfor each label
# "for each line match mnemonic and its parameters"

# recognize segments

# instruction:
# MNEMONIC [OPER1 [, OPER2[, OPER3]]]
