import re

def read(filename):
	try:
		lines = [line.strip() for line in open(filename, "r")]
	except IOError:
		raise IOError('Could not open file for reading')
	return lines;

def write(filename, binary):
	try:
		out_file = open(filename, 'wb')
		out_file.write(binary)
	except IOError:
		raise IOError('Could not open file for writing')
